<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DisciplinasController extends Controller
{
    public function lista() {
   		$results = DB::table('disciplinas')->get();

   		return view('disciplinas.lista', ['disciplinas' => $results]);
    }
}
