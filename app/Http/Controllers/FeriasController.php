<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class FeriasController extends Controller
{
    public function lista() {
   		$results = DB::table('ferias')
   					->select('ferias.*', 'professors.nome', 'disciplinas.descricao')
   					->join('professors', 'professors.professor', '=', 'ferias.professor')
   					->join('disciplinas', 'disciplinas.disciplina', '=', 'professors.disciplina')
   					->get();

   		return view('ferias.lista', ['ferias' => $results]);
    }
}
