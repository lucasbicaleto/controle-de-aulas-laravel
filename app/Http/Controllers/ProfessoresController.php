<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProfessoresController extends Controller
{
   public function lista() {
   		$results = DB::table('professors')
   					->select('professors.*', 'disciplinas.codigo', 'disciplinas.descricao')
   					->join('disciplinas', 'disciplinas.disciplina', '=', 'professors.disciplina')
   					->get();

   		return view('professores.lista', ['professores' => $results]);
   }
}
