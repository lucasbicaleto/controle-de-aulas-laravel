<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AulasController extends Controller
{
    public function lista() {
   		$professores = DB::table('professors')
   							->select('professors.*', 'disciplinas.codigo', 'ferias.data')
   							->join('disciplinas', 'disciplinas.disciplina', '=', 'professors.disciplina')
   							->leftJoin('ferias', 'ferias.professor', '=', 'professors.professor')
							->where('professors.tipo', 'fixo')
							->get()
							->toArray();
   		$substitutos = DB::table('professors')
   							->select('professors.*', 'disciplinas.codigo', 'ferias.data')
   							->join('disciplinas', 'disciplinas.disciplina', '=', 'professors.disciplina')
   							->leftJoin('ferias', 'ferias.professor', '=', 'professors.professor')
							->where('professors.tipo', 'substituto')	
							->get()
							->toArray();
   		$disciplinas = DB::table('disciplinas')->get()->toArray();
   		$ferias      = DB::table('ferias')->get()->toArray();

   		$aulas       = $this->getAulas($professores,$substitutos,$disciplinas,$ferias);
   		$counts      = ['professores'  => count($professores), 
   						'subistitutos' => count($substitutos), 
   						'disciplinas'  => count($disciplinas), 
   						'ferias'       => count($ferias)];

   		return view('welcome', ['aulas' => $aulas, 'counts' => $counts]);
   	}

   	public function getAulas($professores, $substitutos, $disciplinas, $ferias) {

   		/* 
		 * NOTA 1: como era referente apenas ao mes de JUNHO de 2019, obtei por montar na m�o um array simulando os 30 dias de junho
		 *
		 * NOTA 2:
		 * No teste foi citado que: "[...] e todas devem ter de 2 a 3 aulas na semana [..]" que afirma que toda aulas ter�o que ter de 2 a 3 aulas por semana.
		 * Baseando nisso, vamos fazer umas continhas:
			
		   8 = quantitade de disciplinas existentes 
		   5 = quantidade de dia de aulas, na semana
		   4 = quantidade de aula por dia

		   4 x 5 = 20 aulas por semana

		   20 / 8 = 2,5 aulas por semana, b�sicamente cada disciplina entrar� na regra de "2 a 3 aulas na semana"

		  *
  		  *
  		  */ 
   		$aulas = [];
		$mes   = [
			"1" => "seg",
			"2" => "ter",
			"3" => "qua",
			"4" => "qui",
			"5" => "sex",
			"6" => "sab",
			"7" => "dom",
			"8" => "seg",
			"9" => "ter",
			"10" => "qua",
			"11" => "qui",
			"12" => "sex",
			"13" => "sab",
			"14" => "dom",
			"15" => "seg",
			"16" => "ter",
			"17" => "qua",
			"18" => "qui",
			"19" => "sex",
			"20" => "sab",
			"21" => "dom",
			"22" => "seg",
			"23" => "ter",
			"24" => "qua",
			"25" => "qui",
			"26" => "sex",
			"27" => "sab",
			"28" => "dom",
			"29" => "seg",
			"30" => "ter"
		];

		foreach ($mes AS $numero => $dia) {

			/* mistura os professores (pra sempre que atualizar, mostrar resultados diferentes) */
			shuffle($professores);

			/* pega a data atual e compara com a data de f�rias do professores */
			foreach ($professores AS $professor) {	

				/* misturas as  disciplinas (pra sempre que atualizar, mostrar resultados diferentes) */
				shuffle($disciplinas);

				foreach ($disciplinas AS $disciplina) {

					$sigla = $disciplina->codigo;

					/* se o professor n�o antender aquela disciplina, continua */
					if ($professor->codigo != $sigla ) {
						continue;
					}

					/* cria grade vazia para s�bados e domingos */
					if (in_array($dia, ['sab', 'dom'])) {

						$aulas['dia'][$numero]['dia']    = $dia;
						$aulas['dia'][$numero]['fds']    = true;

						/* s� seta como vazio a grade n�o tiver grade cadastrada, como por exemplo de refor�o */
						if (!isset($aulas['dia'][$numero]['grade'])) {
							$aulas['dia'][$numero]['grade']  = [];
						} 
						 
					} else {

						/* repetir grade de aula na semana */
						if ($numero > 0 && $numero <= 7) {

							/* verifico se tem alguma aula cadastrada para esse dia */
							if (isset($aulas['dia'][$numero]['grade'])) {
								
								/* m�ximo 4 aulas por dia */
								if (count($aulas['dia'][$numero]['grade']) <= 3) {

									/* verifica se aula que vai cadastrar j� tem para esse dia */
									foreach ($aulas['dia'][$numero] AS $chave => $grade) {

										/* se n�o for o array da grade, sai fora */
										if ($chave != 'grade') {
											continue;
										}

										/* se a disciplina desse professor, j� estiver listada nesse dia, continua */
										if (isset($grade['disciplina_cod']) && ($grade['disciplina_cod'] == $professor->codigo)) {
											continue;
										}

										$aulas['dia'][$numero]['dia']     = $dia;
										$aulas['dia'][$numero]['grade'][] = [
											'disciplina_cod' => $sigla,
											'disciplina'     => $disciplina->descricao,
											'professor'      => $professor->nome
										];
									}
								}

							} else {

								/* se n�o tem eu cadastro a primeira aula do dia */
								$aulas['dia'][$numero]['dia']      = $dia;
								$aulas['dia'][$numero]['grade'][1] = [
									'disciplina_cod' => $sigla,
									'disciplina'     => $disciplina->descricao,
									'professor'      => $professor->nome
								];
							
							}
						
						} else {

							/* percorre as aulas das primeira semana, para pegar a grade */
							foreach ($aulas['dia'] AS $numeroDia => $arrayAula) {
								
								/* verifica se o dia atual da semana(seg,ter,qua..) � o mesmo que esta na grade */
								if ($dia == $arrayAula['dia']) {

									/* copia a grade desse dia pra proxima semana */
									$aulas['dia'][$numero] = $arrayAula;

									/* Primeiro verifica se o professor esta em f�rias, se estiver, acha um substituto da mesma disciplina */
									if (!empty($professor->data)) {

										$feriasDia = explode("/", $professor->data);

										/* se o dia da aula, for maior que o dia que o professor saiu de f�rias, o subistituo entra */
										if ($numero >= $feriasDia[0]) {

											/* busca um subistituto */
											foreach ($substitutos AS $substituto) {
												
												/* verifica novamente se o subistituto � daquela disciplina */
												if ($substituto->codigo == $professor->codigo ) {

													/* subistitui o professor em ferias */
													foreach ($aulas['dia'][$numero]['grade'] AS $gradeChave => $gradeSub) {

														if ($gradeSub['disciplina_cod'] == $substituto->codigo) {
															$aulas['dia'][$numero]['grade'][$gradeChave]['professor'] = $substituto->nome . " (subistituto)";
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}

					/* AULAS DE REFOR�O */
					$aulasReforco = DB::table('reforcos')
										->select('reforcos.*', 'disciplinas.codigo', 'disciplinas.descricao')
			   							->join('disciplinas', 'disciplinas.disciplina', '=', 'reforcos.disciplina')
			   							->get()->toArray();
					
					if (count($aulasReforco) > 0) {

						/* percorro o array de aulas de reforoco */
						foreach ($aulasReforco AS $dadosReforco) {
		
							/* verifico se o dia de refo�o � igual o dia atual (do foreach ) */
							$dataReforco = explode("/", $dadosReforco->data);

							if ($numero == $dataReforco[0]) {	

								/* verifico se tem alguma aula cadastrada para esse dia */
								if (isset($aulas['dia'][$numero]['grade'])) {

									/* verifico se a disciplina do professor se encaixa na disciplina de refor�o */
									if ($professor->codigo == $dadosReforco->codigo) {
										
										/* se o dia de refor�o for final de semana, seta como final de semana */
										if (in_array($dia, ['sab','dom'])) {
											$aulas['dia'][$numero]['fds']     = true;
										}

										$aulas['dia'][$numero]['dia']     = $dia;
										$aulas['dia'][$numero]['grade'][] = [
											'disciplina_cod' => $dadosReforco->codigo,
											'disciplina'     => $dadosReforco->descricao . " (extra) ",
											'professor'      => $professor->nome
										];
									}


									/* Primeiro verifica se o professor esta em f�rias, se estiver, acha um substituto da mesma disciplina */
									if (!empty($professor->data)) {

										$feriasDia = explode("/", $professor->data);

										/* se o dia da aula, for maior que o dia que o professor saiu de f�rias, o subistituo entra */
										if ($numero >= $feriasDia[0]) {

											/* busca um subistituto */
											foreach ($substitutos AS $substituto) {
												
												/* verifica novamente se o subistituto � daquela disciplina */
												if ($substituto->codigo == $professor->codigo ) {

													

													/* verifico se a disciplina do professor se encaixa na disciplina de refor�o */
													if ($substituto->codigo == $dadosReforco->codigo) {
														
														/* se o dia de refor�o for final de semana, seta como final de semana */
														if (in_array($dia, ['sab','dom'])) {
															$aulas['dia'][$numero]['fds']     = true;
														}

														/* subistitui o professor em ferias */
														foreach ($aulas['dia'][$numero]['grade'] AS $gradeChave => $gradeSub) {

															if ($gradeSub['disciplina_cod'] == $substituto->codigo) {
																$aulas['dia'][$numero]['grade'][$gradeChave]['professor'] = $substituto->nome . " (subistituto)";
															}
														}

													}
												}
											}
										}
									}

								} else {

									/* verifico se a disciplina do professor se encaixa na disciplina de refor�o */
									if ($professor->codigo == $dadosReforco->codigo) {
										
										/* se o dia de refor�o for final de semana, seta como final de semana */
										if (in_array($dia, ['sab','dom'])) {
											$aulas['dia'][$numero]['fds']     = true;
										}

										$aulas['dia'][$numero]['dia']      = $dia;
										$aulas['dia'][$numero]['grade'][1] = [
											'disciplina_cod' => $dadosReforco->codigo,
											'disciplina'     => $dadosReforco->descricao . " (extra) ",
											'professor'      => $professor->nome
										];
									}


									/* Primeiro verifica se o professor esta em f�rias, se estiver, acha um substituto da mesma disciplina */
									if (!empty($professor->data)) {

										$feriasDia = explode("/", $professor->data);

										/* se o dia da aula, for maior que o dia que o professor saiu de f�rias, o subistituo entra */
										if ($numero >= $feriasDia[0]) {

											/* busca um subistituto */
											foreach ($substitutos AS $substituto) {
												
												/* verifica novamente se o subistituto � daquela disciplina */
												if ($substituto->codigo == $professor->codigo ) {													

													/* verifico se a disciplina do professor se encaixa na disciplina de refor�o */
													if ($substituto->codigo == $dadosReforco->codigo) {
														
														/* se o dia de refor�o for final de semana, seta como final de semana */
														if (in_array($dia, ['sab','dom'])) {
															$aulas['dia'][$numero]['fds']     = true;
														}

														/* subistitui o professor em ferias */
														foreach ($aulas['dia'][$numero]['grade'] AS $gradeChave => $gradeSub) {

															if ($gradeSub['disciplina_cod'] == $substituto->codigo) {
																$aulas['dia'][$numero]['grade'][$gradeChave]['professor'] = $substituto->nome . " (subistituto)";
															}
														}
													}
												}
											}
										}
									}

								}
							}

						}
					}
					
				}
			}
		}

		return $aulas;
   	}
}
