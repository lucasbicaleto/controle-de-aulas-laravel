@extends('template.template')

@section('content')

<div class="container-fluid">
    <div class="card mb-4 mt-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            Férias Cadastradas
        </div>
        
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Data</th>
                            <th>Professor</th>
                            <th>Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php  if (count($ferias) > 0) { ?>

                                @foreach ($ferias AS $feriasDados)
                        <?php         
                                echo "<tr> 
                                        <td> ".$feriasDados->ferias." </td>
                                        <td> ".$feriasDados->data." </td>
                                        <td> ".$feriasDados->nome." - (".$feriasDados->descricao.") </td>
                                        <td> <i class='fas fa-edit fa-lg' style='color: #ffc107 !important;'></i> <i class='fas fa-trash fa-lg' style='color: red !important;'></i> </td>
                                     </tr>";
                        ?>
                                @endforeach
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>    

@endsection