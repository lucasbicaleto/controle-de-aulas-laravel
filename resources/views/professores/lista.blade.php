@extends('template.template')

@section('content')

<div class="container-fluid">
	<div class="card mb-4 mt-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            Professores Cadastrados
        </div>
        
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Disciplina</th>
                            <th>Tipo</th>
                            <th>Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php  if (count($professores) > 0) { ?>

                            	@foreach ($professores AS $professorDados)
						<?php         
                                $bg = ($professorDados->tipo == 'fixo') ? "bg-success" : "bg-warning";

                                switch ($professorDados->codigo) {
                                    case 'his': $bg2 = 'bg-new1';    break;
                                    case 'fis': $bg2 = 'bg-new2';    break;
                                    case 'lit': $bg2 = 'bg-info';    break;
                                    case 'geo': $bg2 = 'bg-dark';    break;
                                    case 'ing': $bg2 = 'bg-danger';  break;
                                    case 'mat': $bg2 = 'bg-primary'; break;
                                    case 'por': $bg2 = 'bg-success'; break;
                                    case 'esp': $bg2 = 'bg-warning'; break;
                                    default:    $bg2 = 'bg-success'; break;
                                }

                                echo "<tr> 
                                        <td> ".$professorDados->professor." </td>
                                        <td> ".$professorDados->nome." </td>
                                        <td> <professorDados class='badge rounded-pill $bg2' style='color: #FFF;'> ".$professorDados->descricao."</span> </td>
                                        <td> <span class='badge rounded-pill $bg'  style='color: #FFF;'> ".strtoupper($professorDados->tipo)."</span>     </td>
                                        <td> <i class='fas fa-user-edit fa-lg' style='color: #ffc107 !important;'></i> <i class='fas fa-user-times fa-lg' style='color: red !important;'></i> </td>
                                     </tr>";
                        ?>
                                @endforeach
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>    
@endsection