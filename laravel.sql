-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 24-Jan-2021 às 23:26
-- Versão do servidor: 5.7.32-0ubuntu0.18.04.1
-- PHP Version: 7.0.33-37+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplinas`
--

CREATE TABLE `disciplinas` (
  `disciplina` bigint(20) UNSIGNED NOT NULL,
  `descricao` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codigo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `disciplinas`
--

INSERT INTO `disciplinas` (`disciplina`, `descricao`, `codigo`) VALUES
(1, 'Português', 'por'),
(2, 'Inglês', 'ing'),
(3, 'Espanhol', 'esp'),
(4, 'Literatura', 'lit'),
(5, 'Matemática', 'mat'),
(6, 'Geografia', 'geo'),
(7, 'História', 'his'),
(8, 'Física', 'fis');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ferias`
--

CREATE TABLE `ferias` (
  `ferias` bigint(20) UNSIGNED NOT NULL,
  `data` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `professor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `ferias`
--

INSERT INTO `ferias` (`ferias`, `data`, `professor`) VALUES
(1, '19/06/2019', 1),
(2, '19/06/2019', 5),
(3, '19/06/2019', 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_01_23_174306_create_professors_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `professors`
--

CREATE TABLE `professors` (
  `professor` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disciplina` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `professors`
--

INSERT INTO `professors` (`professor`, `nome`, `tipo`, `disciplina`) VALUES
(1, 'João', 'fixo', 1),
(2, 'Fernando', 'fixo', 2),
(3, 'Marcelo', 'fixo', 3),
(4, 'Maria', 'fixo', 4),
(5, 'Eduarda', 'fixo', 5),
(6, 'Pedro', 'fixo', 6),
(7, 'Silvana', 'fixo', 7),
(8, 'Alberto', 'fixo', 8),
(9, 'Paulo', 'substituto', 1),
(10, 'Renata', 'substituto', 2),
(11, 'Juan', 'substituto', 3),
(12, 'Francisco', 'substituto', 4),
(13, 'Nícolas', 'substituto', 5),
(14, 'Bárbara', 'substituto', 6),
(15, 'Roberto', 'substituto', 7),
(16, 'Thiago', 'substituto', 8);

-- --------------------------------------------------------

--
-- Estrutura da tabela `reforcos`
--

CREATE TABLE `reforcos` (
  `reforco` int(11) NOT NULL,
  `data` varchar(12) NOT NULL,
  `disciplina` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `reforcos`
--

INSERT INTO `reforcos` (`reforco`, `data`, `disciplina`) VALUES
(1, '20/06/2019', 3),
(2, '20/06/2019', 4),
(3, '20/06/2019', 2),
(4, '20/06/2019', 1),
(5, '27/06/2019', 8),
(6, '27/06/2019', 7),
(7, '27/06/2019', 6),
(8, '27/06/2019', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `disciplinas`
--
ALTER TABLE `disciplinas`
  ADD PRIMARY KEY (`disciplina`);

--
-- Indexes for table `ferias`
--
ALTER TABLE `ferias`
  ADD PRIMARY KEY (`ferias`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `professors`
--
ALTER TABLE `professors`
  ADD PRIMARY KEY (`professor`);

--
-- Indexes for table `reforcos`
--
ALTER TABLE `reforcos`
  ADD PRIMARY KEY (`reforco`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `disciplinas`
--
ALTER TABLE `disciplinas`
  MODIFY `disciplina` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ferias`
--
ALTER TABLE `ferias`
  MODIFY `ferias` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `professors`
--
ALTER TABLE `professors`
  MODIFY `professor` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `reforcos`
--
ALTER TABLE `reforcos`
  MODIFY `reforco` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
